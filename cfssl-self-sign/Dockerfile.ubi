## FINAL IMAGE ##

ARG UBI_IMAGE_MICRO=registry.access.redhat.com/ubi9/ubi-micro:9.4

FROM ${UBI_IMAGE_MICRO} as build

RUN mkdir -p /assets
ADD cfssl-self-sign.tar.gz /assets/
COPY scripts/generate-certificates /assets/scripts/generate-certificates

FROM ${UBI_IMAGE_MICRO}

ARG CFSSL_VERSION
ARG FIPS_MODE=0

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/cfssl-self-sign" \
      name="cfssl-self-sign" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${CFSSL_VERSION} \
      release=${CFSSL_VERSION} \
      summary="cfssl-self-sign generates self-signed CA and wildcard certificates certificates" \
      description="cfssl-self-sign generates self-signed CA and wildcard certificates certificates"

COPY --from=build  /assets/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

# generate-certificates script will perform work in this volume
VOLUME /output

CMD ["/scripts/generate-certificates"]

# Default to non-root user
USER 65534:65534
